# Contributing #

Contributions are always welcome. 
Work on your own idea or pick from the list of gitlab [repo issues](https://gitlab.com/groups/tezosisrael/tezos-blockly/-/issues).

## Communicating


## Workflow
The work is done from dev branch. We use merge request workflow driven by issues:
1. Create or pick an issue to work on.
1. Fork the repo to your personal account.
1. Create a new branch, referencing the issue number.
1. Write tests and code.
1. Open a merge request to original repo's dev branch. Reference the issue you are working on in the title.
1. The maintainers will review your contributions and merge them into the repo.

## Tezos resources
This [medium post](https://medium.com/tezos-israel/start-becoming-a-tezos-developer-a-tezos-tools-directory-a59733a5a4f5) lists resources about Tezos blockchain.
We translate Blockly blocks into LIGO smart contract language, in particular LIGO's Reason LIGO flavor. Here are some links about LIGO:

- LIGO's [homepage](https://ligolang.org/ligo)
- [Repo on gitlab](https://gitlab.com/ligolang)
- LIGO's [web ide](https://ide.ligolang.org/) which we copied our backend from.
- Web ide's [source code](https://gitlab.com/ligolang/ligo/-/tree/dev/tools/webide)

## Blockly resources
[Blockly](https://developers.google.com/blockly) is a Javascript library to create code using graphical blocks.

We recommed working through this [codelab](https://blocklycodelabs.dev/codelabs/getting-started/index.html) to get a good understanding of Blockly.

## Tools
We use yarn, including yarn workspaces

## Development server
Webpack development server can be run with `yarn startDev`

## Testing
We are using [Cypress.io](cypress.io) for testing. Locally, start the server as above and then do `yarn cypress open`.
