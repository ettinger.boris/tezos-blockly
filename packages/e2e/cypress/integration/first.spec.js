// first.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
const url = 'localhost:5555';
const TEMPLATES = ['Empty', 'Arithmetic', 'Pay 1tez', 'Safe Subtract'];
describe('Basic Test', () => {
	it('Visits', () => {
		cy.visit(url);
	});
	it('has the right title', () => {
		cy.visit(url);
		cy.title()
		.should('equal', 'Tezos-Blockly - Visual Smart Contract Editor for Tezos protocol');
	});
	it('has the correct contract templates', () => {
		cy.visit(url);
		cy.get('#sel1 option')
			.should('have.length', 4)
	  	.invoke('text').should('equal', TEMPLATES.join(''))
	});
	it('each non empty contract compiles', () => {
		cy.visit(url);
		TEMPLATES.slice(1).forEach(template => {
			cy.get('#sel1').select('Empty');
			cy.get('#sel1').select(template);
			cy.get('#ligo').click();
			cy.get('#result').should('have.text', 'Success!');
		});
	});
})
