export default (Cls) =>{
	return class extends Cls {
		storage(block) {
			var code = 'storage';
			return [code, this.ORDER_NONE];
		}

		return_type(block) {
			//const statements_operations = this.statementToCode(block, 'OPERATIONS');
			const ops = [];
			let nxt = block.getInputTargetBlock('OPERATIONS');
			while (nxt !== null) {
				ops.push(this.blockToCode(nxt, true));
				nxt = nxt.getNextBlock();
			}
			const ops_code = ops.length > 0 ? `[${ops.join(',')}]` : 'noOps';
			const value_storage = this.valueToCode(block, 'STORAGE', this.ORDER_ATOMIC);
			var code = `(${ops_code}, ${value_storage})`;
			return [code, this.ORDER_ATOMIC];
		}

		entry_point(block) {
			var text_entry_point_name = block.getFieldValue('ENTRY_POINT_NAME');
			var value_entry_point_value = this.valueToCode(block, 'ENTRY_POINT_VALUE', this.ORDER_ATOMIC);
			var code = '...;\n';
			return code;
		}

		totez(block) {
			var value_name = this.valueToCode(block, 'NAME', this.ORDER_ATOMIC);
			// TODO: Assemble ReLIGO into code variable.
			var code = `abs(${value_name}) * 1tz`;
			// TODO: Change ORDER_NONE to the correct strength.
			return [code, this.ORDER_NONE];
		}

		pay_to(block) {
			var text_name = block.getFieldValue('NAME');
			var value_name = this.valueToCode(block, 'NAME', this.ORDER_ATOMIC);
			// TODO: Assemble ReLIGO into code variable.
			var code = `payTo((("${text_name}":address), ${value_name}))`;
			return code;
		};
	}
}
